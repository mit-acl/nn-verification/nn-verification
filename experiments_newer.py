import nn_verification.example as ex
import numpy as np
import pandas as pd
import datetime
import os
import glob
import matplotlib.pyplot as plt


results_dir = "{}/results/logs/".format(
    os.path.dirname(os.path.abspath(__file__))
)
os.makedirs(results_dir, exist_ok=True)


class Experiment:
    def __init__(self):
        self.info = {
            ('CROWN', 'Uniform'): {
                'name': 'Reach-LP-Partition',
                'color': 'tab:green',
                'ls': '-',
            },
            ('CROWN', 'None'): {
                'name': 'Reach-LP',
                'color': 'tab:green',
                'ls': '--',
            },
            ('SDP', 'Uniform'): {
                'name': 'Reach-SDP-Partition',
                'color': 'tab:red',
                'ls': '-',
            },
            ('SDP', 'None'): {
                'name': 'Reach-SDP~\cite{hu2020reach}',
                'color': 'tab:red',
                'ls': '--',
            },
            ('SeparableCROWN', 'None'): {
                'name': 'CL-CROWN',
            },
            ('SeparableSGIBP', 'None'): {
                'name': 'CL-SG-IBP~\cite{xiang2020reachable}',
            },
        }


class CompareInputSizeVsErrorTable(Experiment):
    def __init__(self):
        self.filename = results_dir + 'inputset_vs_error_{dt}_table.pkl'
        Experiment.__init__(self)

    def run(self):
        dt = datetime.datetime.now().strftime('%Y_%m_%d__%H_%M_%S')

        parser = ex.setup_parser()
        args = parser.parse_args()

        args.model = "robot_arm"
        args.activation = "relu"
        args.interior_condition = "linf"

        args.save_plot = False
        args.show_plot = False
        args.make_animation = False
        args.show_animation = False
        args.estimate_runtime = False
        args.show_input = True
        args.show_output = True

        expts = [
            {
                'partitioner': 'None',
                'propagator': 'CROWN_LIRPA',
            },
            {
                'partitioner': 'None',
                'propagator': 'IBP_LIRPA',
            },
        ]

        sizes = np.logspace(-3, 2, num=20)

        # sizes = np.array([
        #     [0.10, 0.10],
        #     [0.12, 0.12],
        #     [0.14, 0.14],
        #     [0.18, 0.18],
        #     # [0.2, 0.2],
        #     # [0.3, 0.3],
        #     # [0.4, 0.4],
        #     # [0.5, 0.5],
        #     # [0.6, 0.6],
        #     # [0.7, 0.7],
        #     # [0.8, 0.8],
        # ])
        default_input_range = np.array(
            [  # (num_inputs, 2)
                [np.pi / 3, 2 * np.pi / 3],  # x0min, x0max
                [np.pi / 3, 2 * np.pi / 3],  # x1min, x1max
            ]
        )

        df = pd.DataFrame()

        for size in sizes:
            for expt in expts:
                for key, value in expt.items():
                    setattr(args, key, value)

                # Set input_range (have to write it as string for example.py)
                input_range = np.zeros_like(default_input_range)
                input_range[:, 0] = default_input_range[:, 0]
                input_range[0, 1] = default_input_range[0, 0] + size
                input_range[1, 1] = default_input_range[0, 0]
                # input_range[:, 1] = default_input_range[0, 0] + size
                print(input_range)
                args.input_range = np.array2string(input_range, separator=', ').replace('\n', '')

                stats, info = ex.main(args)

                df = df.append({
                    **expt,
                    'error': stats['error'],
                    'input_range': input_range,
                    'output_range': stats['output_range'],
                    }, ignore_index=True)
        df.to_pickle(self.filename.format(dt=dt))

    def plot(self):

        # Grab latest file as pandas dataframe
        list_of_files = glob.glob(self.filename.format(dt='*'))
        latest_filename = max(list_of_files, key=os.path.getctime)
        df = pd.read_pickle(latest_filename)

        groups = df.groupby('propagator')
        for group in groups:
            propagator, df_ = group

            input_sets = np.array(df_.input_range.to_list())
            input_set_sizes = input_sets[:, 0, 1] - input_sets[:, 0, 0]
            # input_set_sizes = np.product(input_sets[:, :, 1] - input_sets[:, :, 0], axis=1)
            errors = df_.error.to_numpy()
            plt.plot(input_set_sizes, errors, 'x', label=propagator)

        plt.xlabel('Input Set Area')
        plt.ylabel('Output Set Estimation Error')
        plt.semilogx()
        plt.legend()
        plt.tight_layout()

        # Save plot with similar name to pkl file that contains data
        filename = latest_filename
        fig_filename = filename.replace('pkl', 'png')
        plt.savefig(fig_filename)


if __name__ == '__main__':

    # TODO: description
    c = CompareInputSizeVsErrorTable()
    c.run()
    c.plot()

